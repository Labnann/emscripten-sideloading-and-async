#include <emscripten/emscripten.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>

#include <emscripten.h>
#include <emscripten/fetch.h>





int run_loaded_file(){
    void *handle;
    void (*hello_world)();


    
    handle = dlopen("./hello.so", RTLD_LAZY);
    if (!handle) {
        fprintf(stderr, "Error: %s\n", dlerror());
        return 1;
    }

    hello_world = dlsym(handle, "hello_world");
    if (dlerror() != NULL) {
        fprintf(stderr, "Error: %s\n", dlerror());
        dlclose(handle);
        return 1;
    }

    hello_world();

    dlclose(handle);

    return 0;

}


int main() {
    printf("Before sleeping\n");
    emscripten_sleep(5000);
    run_loaded_file();

    return 0;
}

