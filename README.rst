ASYNCIFY AND DOWNLOAD OF OBJECT FOR SIDELOADING
===============================================

It might be unnecessary to download an object in order to sideload
via dlopen.

This prototype shows that case.

Usage:
------
- run `$ sh build-emscripten.sh`

- Now: Run use `python -m http.server` and open build-emscripten-browser/main.html
to run the code. Note that we are using python's http-server in order to
check when hello.so is being requested.


Observation
-----------
Even though the code main.c does not instruct the download of hello.so, 
hello world is still printed. The code sleeps for 5 seconds, then upon 
dlopen being called, a xhr is made to get hello.so (At this time, an 
entry is created in the terminal saying the file is requested). Then the
file is loaded and run, displaying "hello world".

Additional Information
----------------------
This works: when `-sASYNCIFY` flag is used when compilation. This flag
is necessary to use emscripten_wget (which is synchronous).

Without -sASYNCIFY flag, we do need to download the shared object into 
wasmfs to get sideloading working.
